# Scale

## Install

```
pip install git+ssh://git@gitlab.com/jabaliesrc/scale.git@master
```

## Use

```
scale --video JRC-SANMA-11-09.mp4 --width 960
```

will scale the video to 960px width and output to the same directory.
