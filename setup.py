import os
from distutils.core import setup
from setuptools import find_packages

required = [
    'click',
    'ffmpeg-python',
]

setup(
    name='Scale',
    version='0.1',
    author=u'Teofilo Sibileau',
    author_email='teo.sibileau@gmail.com',
    license='MIT license, see LICENSE',
    description='Concatenates video files',
    packages=['src'],
    include_package_data=True,
    zip_safe=False,
    entry_points = {
        'console_scripts': [
            'scale = src.scale:scale',
        ],
    },
    install_requires=required,
)