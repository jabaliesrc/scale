import os
import ffmpeg
import click
from pathlib import Path


def validate_video(ctx, param, value):
    p = Path(value)
    if not p.is_file():
        raise click.BadParameter('video needs to be a file')

    if not p.suffix == '.mp4':
        raise click.BadParameter('We only support mp4 files ATM')

    return value


@click.command()
@click.option('--video', required=True, callback=validate_video,
              help='Path to folder with videos.')
@click.option('--width', default=960, help='With in pixels.')
def scale(video, width):
    p = Path(video)
    output_file = f'{p.parent}/{p.name}.escalado.mp4'

    out, err = (
          ffmpeg.input(video)
                .output(output_file, vf=f'scale={width}:-2')
                .run()
    )

if __name__ == '__main__':
    scale()